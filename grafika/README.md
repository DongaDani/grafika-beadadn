Számítógép Grafika beadandó
Donga Dániel , PIUMNI 

## Feladatleírás: 
A feladat leírásával kapcsolatban az lenne az elképzelésem , hogy az alap koncepció az volt, hogy egy versenypálya célegyenesét reprezentáljam, 
rajta több csúcskategóriás versenyautóval mellé egy cél lámpával , illetve szalagkorláttal + egy folyamatosan mozgó, éppen oldalról "bevágó" autó, aki az első helyre ér. 
A verseny egy szép esti időpontban zajlik , egy csillagos égbolt, és hold alatt.
 A hold erőssége állítható, hogy mennyire világítsa meg a pályát, illetve mellé köd-öt lehet szimulálni a versenypályára.


## Funkciók 
- Köd szimulálása
- Irányítható néhez, és magasság.
- Versenyautó objektumok betöltése, mellé egy csillagos égbolt, és egy hold.
- Hold fényességének növelés, csökkentés.
-help menü
- Camaro jármű elindítása 



## Használati útmutató

1. w,a,s,d billentyű kombinációval lehet elősre, balra, hátra, jobbra menni.
2. Bal klikk + egérforgatással lehet forgatni.
3. Q és E betűvel lehet a magasságot állítani.
4. köd-öt az f-betűvel lehet előidézni.
5. hold fényereének csökkentése : "-" , növelése "+" .
6. help menü előhozása "t" betű hatására.
7. camaro elindítása "k" betű megnyomásával


##A program elindításához szükségesek az alábbi lépések :

Assets mappa letöltése , benne az objektumokkal és a textúrákkal. Ennek az elérési utvonala : 
https://drive.google.com/drive/folders/1vYnEuTl2aP3sVGblaSTTNYdiifxB7D_K?fbclid=IwAR34Fm486UCqOBS6jRNJGxLdQEvfR4G7UuPWdeAQbamMKd6FMdGgOVDc4cM
Az assets mappát az exe file mellé kell másolni,a strukturálságot megőrizve.

## Telepítés:

A grafika_sdk mappába belépve futtatni kell a shell-t majd belépni a könyvtárba és a következő parancsokat kell beírni : 
"make"
"race"

