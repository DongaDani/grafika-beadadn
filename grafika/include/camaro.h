#ifndef CAMARO_H
#define CAMARO_H

#include "camera.h"
#include "texture.h"
#include "utils.h"
#include <obj/model.h>

typedef struct
{
	double startCamaro;
	double speed;
}Camaro;

void camaroMove(Camaro* camaro ,double time);

#endif /* CAMEL_H */