#ifndef SCENE_H
#define SCENE_H

#include "camera.h"
#include "texture.h"
#include "moon.h"
#include "car.h"
#include "camaro.h"

#include <obj/model.h>

typedef struct Scene
{
    Camera camera;
	Model road;
    Model skyBox;
    Model bugatti;
	Model camaro;
	Model moon;
	Model camaro2;
	Model barrier;
	Model traffic;
	Model sls;
    Material material;
	GLuint troad;
	GLuint fu;
	GLuint tsky;
	GLuint tbugatti;
	GLuint tcamaro;
    GLuint help;

	GLuint tmoon;
	GLuint tcamaro2;
	GLuint tbarrier;
	GLuint ttraffic;
	GLuint tsls;
} Scene;

/**
 * Initialize the scene by loading models.
 */
void init_scene(Scene* scene);

/**
 * Set the lighting of the scene.
 */
void set_lighting();

/**
 * Set the current material.
 */
void set_material(const Material* material);

/**
 * Draw the scene objects.
 */
void moveDrawing(const Scene* scene,Rotate* rotate,Camaro* camaro,Car* car);




#endif /* SCENE_H */
