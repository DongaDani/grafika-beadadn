#ifndef MOON_H
#define MOON_H

#include "camera.h"
#include "texture.h"
#include "utils.h"
#include <obj/model.h>

typedef struct
{
	double moon_rotation;
	GLboolean rotate_obj;
}Rotate;

void rotateObject(Rotate *rotate);


#endif 