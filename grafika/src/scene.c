#include "scene.h"
#include "callbacks.h"
#include "moon.h"
#include "camaro.h"
#include "car.h"
#include <GL/glut.h>
#include <obj/load.h>
#include <obj/draw.h>


Rotate rotate;
void init_scene(Scene* scene)
{
    load_model(&(scene->road), "assets/models//road.obj");
    load_model(&(scene->skyBox), "assets/models//night.obj");
    load_model(&(scene->bugatti), "assets/models//bugatti8.obj");
    load_model(&(scene->camaro), "assets/models//camaro3.obj");
	
	load_model(&(scene->moon), "assets/models//moon.obj");

 	load_model(&(scene->camaro2), "assets/models//camaro2.obj");
	load_model(&(scene->barrier), "assets/models//barrier4.obj");
	load_model(&(scene->traffic), "assets/models//trafficlight6.obj");
	load_model(&(scene->sls), "assets/models//sls_amg15.obj");
   
	scene->troad = load_texture("assets/textures//road.jpg");
	scene->tsky = load_texture("assets/textures//sky.jpg");
	scene->tbugatti = load_texture("assets/textures//bugatti.jpg");
	scene->tcamaro = load_texture("assets/textures//camaro.jpg");
    scene->help = load_texture("assets//help.png");
	scene->tmoon = load_texture("assets/textures//moon.jpg");

	scene->tcamaro2 = load_texture("assets/textures//camaroRed.jpg");
	scene->tbarrier = load_texture("assets/textures//barrier.png");
	scene->ttraffic = load_texture("assets/textures//traffic.jpg");
	scene->tsls = load_texture("assets/textures//sls_amg.jpg");
   

  scene->material.ambient.red = 0.6;
  scene->material.ambient.green = 0.6;
  scene->material.ambient.blue = 0.6;

  scene->material.diffuse.red = 1;
  scene->material.diffuse.green = 1;
  scene->material.diffuse.blue = 1;

  scene->material.specular.red = 0.8;
  scene->material.specular.green = 0.8;
  scene->material.specular.blue = 0.8;
    
	scene->material.shininess = 20.0;

	set_lighting(scene);
    
}



void set_material(const Material* material)
{
    float ambient_material_color[] = {
        material->ambient.red,
        material->ambient.green,
        material->ambient.blue
    };

    float diffuse_material_color[] = {
        material->diffuse.red,
        material->diffuse.green,
        material->diffuse.blue
    };

    float specular_material_color[] = {
        material->specular.red,
        material->specular.green,
        material->specular.blue
    };

    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ambient_material_color);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuse_material_color);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular_material_color);

    glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, &(material->shininess));
}



void mooveBugatti(Car* car,double time) {
    car->bugattiY-=3*time;
}

void camaroMove(Camaro * camaro,double time) {
    camaro->startCamaro +=time;
}

void set_lighting()
{
	float ambient_light[] = { 0.6f, 0.6f, 0.6f, 0.6f };
	float diffuse_light[] = { 0.6f, 0.6f, 0.6f, 0.6f };
	float specular_light[] = { 0.6f, 0.6f, 0.6f, 0.6f };
	float position[] = { 0.0f, 0.0f, 15.0f, 1.0f };

	float pos[4] = { 0.f,5.f,1.f,100.f };

	glLightfv(GL_LIGHT0, GL_AMBIENT, ambient_light);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse_light);
	glLightfv(GL_LIGHT0, GL_SPECULAR, specular_light);
	//glLightfv(GL_LIGHT0, GL_POSITION, position);
	glLightfv(GL_LIGHT1, GL_POSITION, pos);
}

void rotateObject(Rotate *rotate)
{
	if (rotate->rotate_obj == TRUE)
	{
		rotate->moon_rotation += 0.05;
	}
}

void moveDrawing(const Scene* scene, Rotate* rotate,Camaro * camaro,Car* car)
{
		set_material(&(scene->material));
        glPushMatrix();
		glTranslatef(0, 0, 0);
		glBindTexture(GL_TEXTURE_2D,scene->troad);
		draw_model(&(scene->road));
		glPopMatrix();
		glPushMatrix();
		glTranslatef(30, 50, 30);
		glRotatef(rotate->moon_rotation, 1, 1, 1);
		glBindTexture(GL_TEXTURE_2D,scene->tmoon);
		draw_model(&(scene->moon));
		glPopMatrix();	
		glPushMatrix();
		glTranslatef(0,0,0);
		glBindTexture(GL_TEXTURE_2D,scene->tsky);
		draw_model(&(scene->skyBox));
		glPopMatrix();
		glPushMatrix();
		glTranslatef(20, 16 - camaro->startCamaro, 0.55);
		glBindTexture(GL_TEXTURE_2D, camaro->startCamaro);
		draw_model(&(camaro->startCamaro));
		glPopMatrix();
        glPushMatrix();
	    glRotatef(70+car->bugattiMove, 0, 0, 1);
		glTranslatef(-25-car->bugattiX, -25-car->bugattiY, 0.5);
		glBindTexture(GL_TEXTURE_2D,scene->tbugatti);
		draw_model(&(scene->bugatti));
		glPopMatrix();	
		glPushMatrix();
		glTranslatef(-14, 12, 0.1);
		glPopMatrix();
		glPushMatrix();
		glTranslatef(-18, -8, 0.3);
		glPopMatrix();		
		glPushMatrix();
		glTranslatef(18, -10, 0.3);
		glPopMatrix();	
		glPushMatrix();
		glTranslatef(0, -22, 0.3);
		glBindTexture(GL_TEXTURE_2D,scene->tsls);
		draw_model(&(scene->sls));
		glPopMatrix();	
		glPushMatrix();
		glTranslatef(20, 22, 0.3);
		glBindTexture(GL_TEXTURE_2D,scene->tcamaro2);
		draw_model(&(scene->camaro2));
		glPopMatrix();	
		glPushMatrix();
		glBindTexture(GL_TEXTURE_2D,scene->tbarrier);
		draw_model(&(scene->barrier));
		glPopMatrix();
		glPushMatrix();
		glBindTexture(GL_TEXTURE_2D,scene->ttraffic);
		glTranslatef(20, 16-camaro->startCamaro, 2.4);
		draw_model(&(scene->traffic));
		glPopMatrix();
		
		glBindTexture(GL_TEXTURE_2D,scene->help); 
		
		
}
		
		
        

		
	